![visitors](https://visitor-badge.glitch.me/badge?page_id=BigoudOps.readme)


---
# this is a theme for ventoy.

For more information about this ➡ <https://www.ventoy.net/en/plugin_theme.html>
& <https://www.ventoy.net/en/plugin_menuclass.html>
---
This theme is compatible with version 1.0.46 of Ventoy.

![capture](src/firstboot.png)

## In this repo you have:
1. font custom waltographUI converted from .ttf to .pf2 original file here <https://www.dafont.com/fr/waltograph.font>
2. Complete icon themes for windows and linux distributions.

I created in the Ventoy partition an ISO folder in which it is located three folders:
1. `Linux` (For Linux ISO)
2. `Repair tools` (To integrate disk repair tools etc)
3. `Windows` (For Windows ISO)

**View as list**   
![capture](src/listiso.png)
**Power menu display**
![capture](src/powersection.png)
**Display of the tools menu**
![capture](src/toolsection.png)
